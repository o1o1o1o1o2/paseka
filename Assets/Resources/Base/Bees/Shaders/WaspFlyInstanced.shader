﻿Shader "AnimationInstancing/WaspFlyInstanced"
{
Properties {
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	_Color ("Color", Color) = (1,1,1,1)
	[NoScaleOffset]	_BumpMap ("Normalmap", 2D) = "bump" {}
}

CGINCLUDE

#include <Lighting.cginc>

#include "Assets\Scripts\Extra\AnimationInstancing\AniInstancing\Shader\AnimationInstancingBase.cginc"

sampler2D _MainTex;
sampler2D _BumpMap;
fixed4 _Color;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap; 
};



 void vert(inout appdata_full v)
{      
#ifdef UNITY_PASS_SHADOWCASTER
	v.vertex = skinningShadow(v);
#else
	v.vertex = skinning(v);
#endif
}

fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
{
	 fixed4 c;
	 c.rgb = s.Albedo; 
	 c.a = s.Alpha;
	 return c;
}

void surf (Input IN, inout SurfaceOutput o) {	
	o.Albedo = tex2D(_MainTex, IN.uv_MainTex)*_Color;
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
ENDCG

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 200
	
	CGPROGRAM
	#pragma surface surf NoLighting 
	#pragma vertex vert 
	#pragma multi_compile_instancing
   
	ENDCG
}
}
