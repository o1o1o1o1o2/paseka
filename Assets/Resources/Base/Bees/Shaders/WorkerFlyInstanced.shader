﻿Shader "AnimationInstancing/BeeWorkerFlyInstanced"
{
Properties {
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
	[NoScaleOffset]	_BumpMap ("Normalmap", 2D) = "bump" {}
	_DisplacementAmount("Displacement Amount", float) = 1
    _DisplacementSpeed("Displacement Speed", float ) = 1
}

CGINCLUDE

#include <Lighting.cginc>

#include "Assets\Scripts\Extra\AnimationInstancing\AniInstancing\Shader\AnimationInstancingBase.cginc"

sampler2D _MainTex;
sampler2D _BumpMap;
fixed4 _Color;
half _Shininess;

struct Input {
	float2 uv_MainTex;
	float2 uv_BumpMap; 
};

float _DisplacementAmount;
float _DisplacementSpeed;

 void vert(inout appdata_full v)
{      
#ifdef UNITY_PASS_SHADOWCASTER
	v.vertex = skinningShadow(v);
#else
	v.vertex = skinning(v);
#endif
    v.vertex.y += sin(_Time.y * _DisplacementSpeed) * _DisplacementAmount ;
}

fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
{
	 fixed4 c;
	 c.rgb = s.Albedo; 
	 c.a = s.Alpha;
	 return c;
}

void surf (Input IN, inout SurfaceOutput o) {	
	o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
	o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
}
ENDCG

SubShader { 
	Tags { "RenderType"="Opaque" }
	LOD 200
	
	CGPROGRAM
	#pragma surface surf NoLighting 
	#pragma vertex vert 
	#pragma multi_compile_instancing
   
	ENDCG
}
}
