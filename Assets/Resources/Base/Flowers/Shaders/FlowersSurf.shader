Shader "Polygon Wind/FlowersSurf" {
 
    Properties {
        _MainTex ("Main Texture", 2D) = "white" {}
        _Tint ("Tint", Color) = (1,1,1,1)

        _wind_dir ("Wind Direction", Vector) = (0.5,0.05,0.5,0)
        _tree_sway_stutter_influence("Tree Sway Stutter Influence", range(0,1)) = 0.2
        _tree_sway_stutter ("Tree Sway Stutter", range(0,10)) = 1.5
        _tree_sway_speed ("Tree Sway Speed", range(0,10)) = 1
        _tree_sway_disp ("Tree Sway Displacement", range(0,1)) = 0.3
    }
 
    SubShader {
        
        CGPROGRAM
        #pragma target 3.0
        #pragma surface surf Lambert vertex:vert addshadow

        float4 _wind_dir;
        float _tree_sway_speed;
        float _tree_sway_disp;
        float _tree_sway_stutter;
        float _tree_sway_stutter_influence;


        sampler2D _MainTex;
        fixed4 _Tint;


        struct Input {
            float2 uv_MainTex : TEXCOORD0;
        };

 
        // Vertex Manipulation Function
        void vert (inout appdata_base i) {

            i.vertex = mul(unity_ObjectToWorld, i.vertex);

            //Tree Movement and Wiggle
            i.vertex.x += (cos(_Time.z * _tree_sway_speed  + (sin(_Time.z * _tree_sway_stutter * _tree_sway_speed) * _tree_sway_stutter_influence)) + 1)/2 * _tree_sway_disp * _wind_dir.x * (i.vertex.y / 10) ; 

            i.vertex.z += (cos(_Time.z * _tree_sway_speed  + (sin(_Time.z * _tree_sway_stutter * _tree_sway_speed) * _tree_sway_stutter_influence)) + 1)/2 * _tree_sway_disp * _wind_dir.y * (i.vertex.y / 10) ;

            i.vertex.z += cos(_Time.z * _tree_sway_speed) * _tree_sway_disp * _wind_dir.y * (i.vertex.y / 10);

            i.vertex = mul(unity_WorldToObject, i.vertex);
        }

        // Surface Shader
        void surf (Input IN, inout SurfaceOutput o) {
            fixed4 col = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = col.rgb;
        }

        ENDCG
        }
    Fallback "Mobile/Diffuse"
} 