using System;
using UnityEngine;

public class GameStatesMachine : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private MainScreenView mainScreenView;
    [SerializeField] private HudScreenView hudScreenView;
    [SerializeField] private StatisticScreenView statisticScreenView;
    
    private StateMachine _stateMachine;
    private void Awake()
    {
        _stateMachine = new StateMachine();
        
        var statisticScreenState = new StatisticScreenViewController(gameManager,statisticScreenView);
        var hudScreenState = new HudScreenViewController(gameManager,hudScreenView);
        var mainScreenState = new MainScreenViewController(mainScreenView);
        
        AddTransition(mainScreenState, hudScreenState, () => mainScreenState.startGame);
        AddTransition(hudScreenState, statisticScreenState, () => hudScreenState.showStatistic);
        AddTransition(statisticScreenState, hudScreenState, () => statisticScreenState.goBack);

        
        void AddTransition(IState to, IState from, Func<bool> condition) => _stateMachine.AddTransition(to, from, condition);

        _stateMachine.SetState(mainScreenState);
    }

    public Type GetCurrentState()
    {
        return _stateMachine.currentState.GetType();
    }

    void Update() =>_stateMachine.Tick();
    
}
