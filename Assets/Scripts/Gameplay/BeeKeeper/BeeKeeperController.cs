﻿using System;
using System.Collections;
using Fly.Utils;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class BeeKeeperController : MonoBehaviour
{
    public GameManager gameManager;
    
    private Camera _camera;
    [SerializeField] private HouseController _houseController;
    
    [SerializeField] 
    private float movementSpeed;

    [SerializeField] private float cleanRate;
    [SerializeField] private float cleanAmount;
    
    [SerializeField] private float gatherRate;
    [SerializeField] private int gatherAmount;
    
    [SerializeField] private int honeyCapacity;

    [SerializeField, ReadOnlyProperty] private int currentHoneyAmount;
    
    private Animator _animator;
    private NavMeshAgent _navMeshAgent;
    
    private int _walkingId;
    private int _interactingId;

    private HiveController _intarectHive;

    private IEnumerator _currentAction;
    
    private Renderer _renderer;

    private void Awake()
    {
        _camera = Camera.main;
        _renderer = GetComponentInChildren<Renderer>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();

        _navMeshAgent.speed *= movementSpeed;

        _walkingId = Animator.StringToHash("walking");
        _interactingId = Animator.StringToHash("interacting");
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void MoveToScreenPos(Vector2 targetScreenPos)
    {
        _currentAction = null;
        _camera.ScreenPointToHitWorldPosition(targetScreenPos, out Vector3 hitPos);
        MoveToPos(hitPos);
    }
    
    public void MoveToPos(Vector3 position)
    {
        _navMeshAgent.isStopped = false;
        _navMeshAgent.SetDestination(position);
        
        _animator.SetBool(_walkingId, true);
        _animator.SetBool(_interactingId, false);
        StartCoroutine(Walk());
    }
    
    private IEnumerator Walk()
    {
        while (true)
        {
            if (!_navMeshAgent.isStopped && !_navMeshAgent.pathPending && Vector3.Distance ( transform.position , _navMeshAgent.pathEndPosition ) < _navMeshAgent.stoppingDistance)
            {
                _animator.SetBool(_walkingId, false);
                _navMeshAgent.isStopped = true;
                if (_currentAction != null)
                {
                    if (_intarectHive) StartCoroutine(PlayerLookAtTarget(_intarectHive.spawnZonePosition));
                    else StartCoroutine(_currentAction);
                }

                yield break;
            }
            yield return null;
        }
    }
    
    IEnumerator PlayerLookAtTarget(Vector3 position)
    {
        while (true)
        {
            var localTarget = transform.InverseTransformPoint(position);
            var angle = Mathf.Atan2(localTarget.x, localTarget.z) * Mathf.Rad2Deg;
            var eulerAngleVelocity = new Vector3(0, angle, 0);

            if (Mathf.Abs(angle) < 0.1)
            {
                if (_currentAction != null) StartCoroutine(_currentAction);
                yield break;
            }
            
            var deltaRotation = Quaternion.Euler(eulerAngleVelocity * (Time.deltaTime * 10));

            _navMeshAgent.transform.rotation *= deltaRotation;
            yield return null;
        }
    }

    
    private IEnumerator CleanHiveProcess()
    {
        _animator.SetBool(_interactingId, true);
        while (true)
        {
            yield return new WaitForSeconds(cleanRate);
            if (_intarectHive.efficiency < 1)  _intarectHive.AddEfficiency(cleanAmount);
            else
            {
                _animator.SetBool(_interactingId, false);
                yield break;
            }
        }
    }
    
    private IEnumerator GatherHoneyProcess()
    {
        _animator.SetBool(_interactingId, true);
        while (true)
        {
            yield return new WaitForSeconds(gatherRate);
            if (_intarectHive.currentHoneyAmount > 0 && currentHoneyAmount < honeyCapacity)
            {
                currentHoneyAmount += _intarectHive.GiveHoney(Mathf.Min(honeyCapacity - currentHoneyAmount, gatherAmount));
            }
            else
            {
                _intarectHive = null;
                _animator.SetBool(_interactingId, false);
                _currentAction = BringGatheredHoneyToHouse();
                MoveToPos(_houseController.doorTransform.position);
                yield break;
            }
        }
    }
    
    private IEnumerator BringGatheredHoneyToHouse()
    {
        _houseController.AddHoney(currentHoneyAmount);
        currentHoneyAmount = 0;
        yield break;
    }
    
    public void CleanHive(HiveController hiveController)
    {
        StopAllCoroutines();
        _intarectHive = hiveController;
        _currentAction = CleanHiveProcess();
        MoveToPos(_intarectHive.spawnZonePosition);
    }

    public void GatherHoney(HiveController hiveController)
    {
        StopAllCoroutines();
        _intarectHive = hiveController;
        _currentAction = GatherHoneyProcess();
        gameManager.SelectHive(null);
        MoveToPos(_intarectHive.spawnZonePosition);
    }

    public void Select() => _renderer.sharedMaterial.color = Color.green;
    
    public void Deselect() => _renderer.sharedMaterial.color = Color.white;

    private void OnApplicationQuit() => Deselect();
}

