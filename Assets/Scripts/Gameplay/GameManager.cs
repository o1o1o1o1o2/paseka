﻿using AnimationInstancing;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameStatesMachine _gameStatesMachine;
    [SerializeField] private GameplayUIView gameplayUIView;
    private GameplayUIViewController _gameplayViewController;
    
    public BeeKeeperController beeKeeperController;
    public HouseController houseController;
    
    [HideInInspector] public HiveController[] hiveControllers;
    [ReadOnlyProperty]
    public HiveController selectedHive;

    [SerializeField,ReadOnlyProperty]
    private bool _beekeeperSelected;
    public bool beekeeperSelected => _beekeeperSelected;

    private void Awake()
    {
        Time.timeScale = 0;
        hiveControllers = FindObjectsOfType<HiveController>();
        
        _gameplayViewController = new GameplayUIViewController(this,gameplayUIView);
    }

    private void Start()
    {
#if !UNITY_EDITOR
        StartCoroutine( AnimationManager.GetInstance().LoadAnimationAssetBundle(Application.dataPath + "/StreamingAssets/AssetBundle/animationtexture"));
#endif
    }

    public void SelectHive(HiveController hiveController)
    {
        if (selectedHive != null) selectedHive.DeselectHive();
        if (hiveController) hiveController.SelectHive();
        selectedHive = hiveController;
    }

    public void DeselectAll()
    {
        if (selectedHive != null)
        {
            selectedHive.DeselectHive();
            selectedHive = null;
        }
        if (_beekeeperSelected)
        {
            beeKeeperController.Deselect();
            _beekeeperSelected = false;
        }
    }

    public void SelectBeekeper()
    {
        DeselectAll();
        _beekeeperSelected = true;
        beeKeeperController.Select();
    }

    private void Update()
    {
        if (_gameStatesMachine.GetCurrentState() == typeof(HudScreenViewController)) _gameplayViewController.Tick();
    }
}
