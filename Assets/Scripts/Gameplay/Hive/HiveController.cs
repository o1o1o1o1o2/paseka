﻿using System.Collections;
using Extra.SerializableDictionary.MyDictionaries;
using Extra.SimplePool;
using UnityEngine;

#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
#endif

public class HiveController : MonoBehaviour
{
    public bool hiveIsActive = false;
    [SerializeField] private bool startOver;
    
    [HideInInspector]
    public Vector3 spawnZonePosition;
    [SerializeField] private Transform spawnZone;

    [SerializeField]
    private GameEntitySO queenSO;
    [SerializeField] private QueenController _queenController;
    
    [SerializeField]
    private GameEntitySO workerSO;
    public PoolIdPair workerID => workerSO.poolIdPair;
    [SerializeField]
    private GameEntitySO droneSO;
    [SerializeField]
    private GameEntitySO hornetSO;
    
    public int workersNum => beeSpawnableDic[workerSO.poolIdPair].currentNum;
    public int dronesNum => beeSpawnableDic[droneSO.poolIdPair].currentNum;
    public int hornetsNum => beeSpawnableDic[hornetSO.poolIdPair].currentNum;

    [ReadOnlyProperty,SerializeField]
    private int _currentHoneyAmount;
    public int currentHoneyAmount => _currentHoneyAmount;
    
    [SerializeField, Range(0.001f, 1)] private float _efficiency;
    public float efficiency => _efficiency;
    
    [SerializeField] private float efficiencyDecreaseRate;
    [SerializeField] private float efficiencyDecreaseAmount;

    [SerializeField]
    private int maxBeeNum;
    
    [ReadOnlyProperty,SerializeField]
    private int _currentBeeNum;
    public int currentBeeNum => _currentBeeNum;
    
    [ReadOnlyProperty,SerializeField]
    private int _workerBeesOut;
    public int workerBeesOut => _workerBeesOut;
    
    public DictionaryBeeSpawnable beeSpawnableDic;

    [SerializeField]
    private int _honeyCapacity;
    [SerializeField]
    public int honeyCapacity => _honeyCapacity;

    private Renderer _renderer;
    private Material _material;
    
    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        _material = _renderer.sharedMaterial;
        spawnZonePosition = spawnZone.position;

        if (hiveIsActive)
        {
            _currentBeeNum = 0;
            if (startOver)
                foreach (var bee in beeSpawnableDic)
                    beeSpawnableDic[bee.Key].currentNum = 0;
            else
                foreach (var bee in beeSpawnableDic)
                    _currentBeeNum += beeSpawnableDic[bee.Key].currentNum;

            if (maxBeeNum < _currentBeeNum) maxBeeNum = _currentBeeNum;

            QueenSetup();
            StartCoroutine(DecreaseEfficiency());
        }
    }

    public void AddEfficiency(float amount) => _efficiency = Mathf.Min(_efficiency + amount, 1);

    public int GiveHoney(int amount)
    {
        int gatheredAmount = Mathf.Min(_currentHoneyAmount, amount);
        _currentHoneyAmount -= gatheredAmount;
        return gatheredAmount;
    }

    public void AddHoney(int amount) => _currentHoneyAmount = Mathf.Min(_honeyCapacity, _currentHoneyAmount + amount);
    
    private void QueenSetup()
    {
        if (_queenController == null)
        {
            _queenController = Instantiate(queenSO.prefab, gameObject.transform).GetComponent<QueenController>();
            _queenController.SetHiveController(this);
        }
    }

    private IEnumerator DecreaseEfficiency()
    {
        while (true)
        {
            yield return new WaitForSeconds(efficiencyDecreaseRate);
            if (efficiency > 0) _efficiency -= Mathf.Min(efficiencyDecreaseAmount, efficiency - efficiencyDecreaseAmount);
        }
    }

    public void AddBee(PoolIdPair poolIdPair)
    {
        if (_currentBeeNum < maxBeeNum)
        {
            beeSpawnableDic[poolIdPair].currentNum++;
            _currentBeeNum++;
        }
    }

    private void NewQueenHasBorn()
    {
        //todo
    }

    public void WorkerDied(PoolIdPair beeId)
    {
        if (beeId.id == workerID)_workerBeesOut--;
        _currentBeeNum--;
        beeSpawnableDic[beeId].currentNum--;
    }
    
    public void SendBees(PoolIdPair poolIdPair,int beesToSendNum)
    {
        int beesCamBeSend = Mathf.Min(beeSpawnableDic[poolIdPair].currentNum - _workerBeesOut, beesToSendNum);//todo _workerBeesOut change to dictionary item
        _workerBeesOut += beesCamBeSend;

        for (int i = 0; i < beesCamBeSend; i++)
        {
            ((IPoolable<HiveController>)SimplePool.Instance.GetFromPool(workerSO)).OnPoolDequeue( this);
        }
    }

    public bool HiveIsFullOfHoney()
    {
        return _currentHoneyAmount >= _honeyCapacity;
    }

    public void WorkerBeeHasReturned() => _workerBeesOut--;

    public void SelectHive()
    {
       _renderer.material.color = Color.red;
        //todo set material withoutline
    }
    
    public void DeselectHive() =>_renderer.sharedMaterial = _material;
    
}

#if UNITY_EDITOR
[CustomEditor(typeof(HiveController))]
[CanEditMultipleObjects]
public class LookAtPointEditor : Editor 
{
    private SerializedProperty _workerProp;
    private SerializedProperty _droneProp;
    private SerializedProperty _hornetProp;
    
    private GameEntitySO _worker;
    private GameEntitySO _drone;
    private GameEntitySO _hornet;

    
    private DictionaryBeeSpawnable beeSpawnableDic;

    void OnEnable()
    {
        _workerProp = serializedObject.FindProperty("workerSO");
        _droneProp = serializedObject.FindProperty("droneSO");
        _hornetProp = serializedObject.FindProperty("hornetSO");
        beeSpawnableDic =  SerializedPropertyExtensions.SerializedPropertyToObject<DictionaryBeeSpawnable>(serializedObject.FindProperty("beeSpawnableDic"));
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        serializedObject.Update();

        if (_workerProp.objectReferenceValue != null) _worker = (GameEntitySO)_workerProp.objectReferenceValue;
        else  _worker = null;

        if (_droneProp.objectReferenceValue != null) _drone = (GameEntitySO)_droneProp.objectReferenceValue;
        else _drone = null;

        if (_hornetProp.objectReferenceValue != null) _hornet = (GameEntitySO)_hornetProp.objectReferenceValue;
        else _hornet = null;
        
        var keys = beeSpawnableDic.Keys.ToArray();
        foreach (var key in keys)
        {
            if (_worker != null) if (key.Equals(_worker.poolIdPair)) continue;
            if (_drone != null) if (key.Equals(_drone.poolIdPair)) continue;
            if (_hornet != null) if (key.Equals(_hornet.poolIdPair)) continue;
            beeSpawnableDic.Remove(key);
        }
        
        if (_worker != null && !beeSpawnableDic.ContainsKey(_worker.poolIdPair)) beeSpawnableDic.Add(_worker.poolIdPair,new BeeSpawnable(_worker, _worker.spawnChance, (HiveController)serializedObject.targetObject));
        if (_drone != null && !beeSpawnableDic.ContainsKey(_drone.poolIdPair)) beeSpawnableDic.Add(_drone.poolIdPair,new BeeSpawnable(_drone, _drone.spawnChance, (HiveController)serializedObject.targetObject));
        if (_hornet != null && !beeSpawnableDic.ContainsKey(_hornet.poolIdPair)) beeSpawnableDic.Add(_hornet.poolIdPair,new BeeSpawnable(_hornet, _hornet.spawnChance, (HiveController)serializedObject.targetObject));

        serializedObject.ApplyModifiedProperties();
    }
}

#endif
