using System;

[Serializable]
public class FlowerSpawnData
{
    public IntReference honeyCapacity;
    public FloatReference honeyReplenishRate;
    public IntReference honeyReplenishAmount;
    public IntReference growSpeed;
    public IntReference maxBeeNum;
}
