﻿using System.Linq;
using Extra.SerializableDictionary.MyDictionaries;
using Logic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpawnController))]
public class FlowerManager : SpawnManager<FlowerController,FlowerManager>
{
    private NativeHashMap<int,float3> _activeFlowerPositions;
    
    [ReadOnlyProperty,SerializeField]
    private int _spawnedFlowersNum;

    [SerializeField]
    private DictionaryFlowerSpawnable flowerTypes;

    protected override void Awake()
    {
        base.Awake();
        _activeFlowerPositions = new NativeHashMap<int, float3>(maxItemsNum, Allocator.Persistent);
        InitSpawner();
    }

    private void OnApplicationQuit() =>_activeFlowerPositions.Dispose();

    protected void InitSpawner()
    {
        _spawnController = GetComponent<SpawnController>();
        currentItemsNum = 0;
        
        _spawnController.SetupSpawneritems(flowerTypes.Values.ToArray());
        _spawnController.StartSpawn(spawnRate);
    }
    
    public bool CanSpawnFlower(PoolIdPair poolIdPair)
    {
        if (_spawnedFlowersNum < maxItemsNum) return true;
        return false;
    }

    public void IncrementSpawnedFlowerNum(PoolIdPair poolIdPair)
    {
        _spawnedFlowersNum++;
        flowerTypes[poolIdPair].currentNum++;
    }

    public override void AddItem(FlowerController item)
    {
        base.AddItem(item);
        _activeFlowerPositions.TryAdd(item.instanceId, item.transform.position);
    }

    public override void RemoveItem(FlowerController item)
    {
            base.RemoveItem(item);
            _activeFlowerPositions.Remove(item.instanceId);
            flowerTypes[item.poolIdPair].currentNum--;
            _spawnedFlowersNum--;
    }

    public NativeHashMap<int,float3> GetFlowerPositions()
    {
        return _activeFlowerPositions;
    }
    
    public bool GetRandomFlowerPosition(out Vector3 position)
    {
        position = Vector3.zero;
        if (itemsNum > 0)
        {
            int idx = Random.Range(0, itemsNum);
            position = items[items.Keys.ElementAt(idx)]._gatheringTransform.position;
            return true;
        }
        return false;
    }

    private void Update() =>_spawnController.SetSpawnRate(spawnRate + (float)itemsNum/maxItemsNum); //todo div by zero
}
