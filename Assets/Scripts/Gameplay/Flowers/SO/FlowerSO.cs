﻿using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Flower", menuName = "Entities/SpawnableFlower", order = 1)]
public class FlowerSO : GameEntitySO
{
    #if UNITY_EDITOR
    [SerializeField]
    private FlowerSO _prototype;
    [SerializeField, InspectorButton("SetupFromPrototype")]
    private bool setupFromPrototype;

    private void SetupFromPrototype()
    {
        _flowerSpawnData = new FlowerSpawnData();
        _flowerSpawnData.growSpeed = new IntReference();
        _flowerSpawnData.honeyCapacity  = new IntReference();
        _flowerSpawnData.honeyReplenishAmount = new IntReference();
        _flowerSpawnData.honeyReplenishRate = new FloatReference();
        _flowerSpawnData.maxBeeNum = new IntReference();

        _flowerSpawnData.growSpeed.useConstant = _prototype.flowerSpawnData.growSpeed.useConstant;
        _flowerSpawnData.honeyCapacity.useConstant =_prototype.flowerSpawnData.honeyCapacity.useConstant;
        _flowerSpawnData.honeyReplenishAmount.useConstant = _prototype.flowerSpawnData.honeyReplenishAmount.useConstant;
        _flowerSpawnData.honeyReplenishRate.useConstant = _prototype.flowerSpawnData.honeyReplenishRate.useConstant;
        _flowerSpawnData.maxBeeNum.useConstant = _prototype.flowerSpawnData.maxBeeNum.useConstant;

        _flowerSpawnData.growSpeed.variable = (!_flowerSpawnData.growSpeed.useConstant)? _prototype.flowerSpawnData.growSpeed.variable:null;
        _flowerSpawnData.honeyCapacity.variable =(!_flowerSpawnData.growSpeed.useConstant)? _prototype.flowerSpawnData.honeyCapacity.variable:null;
        _flowerSpawnData.honeyReplenishAmount.variable = (!_flowerSpawnData.growSpeed.useConstant)? _prototype.flowerSpawnData.honeyReplenishAmount.variable:null;
        _flowerSpawnData.honeyReplenishRate.variable = (!_flowerSpawnData.growSpeed.useConstant)? _prototype.flowerSpawnData.honeyReplenishRate.variable:null;
        _flowerSpawnData.maxBeeNum.variable = (!_flowerSpawnData.maxBeeNum.useConstant)? _prototype.flowerSpawnData.maxBeeNum.variable:null;
        EditorUtility.SetDirty(this);
    }
#endif

    [SerializeField] FlowerSpawnData _flowerSpawnData;
    public FlowerSpawnData flowerSpawnData => _flowerSpawnData;

   

}
