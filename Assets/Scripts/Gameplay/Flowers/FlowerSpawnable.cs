﻿using System;
using Extra.SimplePool;
using UnityEngine;

[Serializable]
public class FlowerSpawnable : Spawnable
{
    [SerializeField]
    private FlowerSpawnData spawnData;
    public FlowerSpawnable(IPoolableData poolableData, float spawnChance, FlowerSpawnData data) : base(poolableData, spawnChance)
    {
        spawnData = data;
    }

    public override void Spawn()
    {
        if (FlowerManager.Instance.CanSpawnFlower(poolIdPair))
        {
            FlowerManager.Instance.IncrementSpawnedFlowerNum(poolIdPair);
            ((IPoolable<FlowerSpawnData>)SimplePool.Instance.GetFromPool(this)).OnPoolDequeue(in spawnData);
        }
    }
}
