﻿using System.Collections;
using Extra.SimplePool;
using UnityEngine;
using Random = UnityEngine.Random;

public class FlowerController : EntitieController, IPoolable<FlowerSpawnData>
{
    public bool isAlive;
    [SerializeField]
    public Transform _gatheringTransform;
    [SerializeField,ReadOnlyProperty]
    private int _honeyCapacity;
    [SerializeField,ReadOnlyProperty]
    private int _currentHoneyAmount;
    [SerializeField,ReadOnlyProperty]
    private int _growSpeed;
    [SerializeField,ReadOnlyProperty]
    private float _honeyReplenishRate;
    [SerializeField,ReadOnlyProperty]
    private int _honeyReplenishAmount;
    [SerializeField,ReadOnlyProperty]
    private int _maxBeeNum;
    [SerializeField,ReadOnlyProperty]
    private int _currentBeeNum;

    public void OnPoolDequeue(in FlowerSpawnData hiveController)
    {
        _honeyCapacity = hiveController.honeyCapacity;
        _currentHoneyAmount = _honeyCapacity;
        _growSpeed = hiveController.growSpeed;
        _honeyReplenishAmount = hiveController.honeyReplenishAmount;
        _honeyReplenishRate = hiveController.honeyReplenishRate;
        _maxBeeNum = hiveController.maxBeeNum;
        _currentBeeNum = 0;
        
        _transform.position = new Vector3(Random.Range(FlowerManager.Instance.spawnZoneRect.xMin, FlowerManager.Instance.spawnZoneRect.xMax),
            0,
            Random.Range(FlowerManager.Instance.spawnZoneRect.yMin, FlowerManager.Instance.spawnZoneRect.yMax));

        var q = _transform.rotation.eulerAngles;

        _transform.rotation = Quaternion.Euler(q.x, Random.Range(0, 360), q.z);

        transform.localScale = Vector3.zero;
        StartCoroutine(Grow());
    }

    public int GiveHoney(int amount)
    {
        if (_currentHoneyAmount > 0)
        {
            var amountGathered = Mathf.Min(_currentHoneyAmount, amount);
            _currentHoneyAmount -= amountGathered;
            if (_currentHoneyAmount == 0) Deactivate();
            return amountGathered;
        }
        return 0;
    }

    public bool AddBee()
    {
        if (_currentBeeNum < _maxBeeNum)
        {
            _currentBeeNum++;
            return true;
        }
        return false;
    }

    public void RemoveBee() =>  _currentBeeNum--;

    private void Deactivate()
    {
        isAlive = false;
        StopAllCoroutines();
        FlowerManager.Instance.RemoveItem(this);
        SimplePool.Instance.ReturnToPool(this);
    }


    private IEnumerator Grow()
    {
        float i = 0;
        while (true)
        {
            i += Time.deltaTime / _growSpeed;
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, i);
            if (i > 1)
            {
                isAlive = true;
                FlowerManager.Instance.AddItem(this);
                yield return ReplenishHoney();
            }
            yield return null;
        }
    }
    
    public IEnumerator ReplenishHoney()
    {
        while (true)
        {
            yield return new WaitForSeconds(_honeyReplenishRate);
            if (_currentHoneyAmount < _honeyCapacity) _currentHoneyAmount = Mathf.Min(_currentHoneyAmount + _honeyReplenishAmount, _honeyCapacity);
        }
    }
}
