﻿using System.Collections.Generic;
using System.Linq;
using Logic;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class SpawnManager<T,V> :  Singleton<V> where V: SpawnManager<T,V> where T : EntitieController
{
    protected Dictionary<int,T> items;
    
    public int itemsNum => items.Count;
    
    [SerializeField] private GameObject spawnZone;

    protected Rect _spawnZoneRect;
    public Rect spawnZoneRect => _spawnZoneRect;
    
    [SerializeField]
    protected int maxItemsNum;

    [SerializeField, ReadOnlyProperty] protected int currentItemsNum;

    [SerializeField]
    protected float spawnRate;
    
    protected SpawnController _spawnController;

    protected virtual void Awake()
    {
        currentItemsNum = 0;
        items = new Dictionary<int, T>(maxItemsNum);
        SetupSpawnZone();
    }

    private void SetupSpawnZone()
    {
        var position = spawnZone.transform.position;
        var localScale = spawnZone.transform.localScale;
        _spawnZoneRect =  new Rect(position.x - localScale.x/2, position.z - localScale.z/2, localScale.x, localScale.z);
    }
    
    public virtual void AddItem(T item)
    {
        items.Add(item.instanceId, item);
        currentItemsNum++;
    }

    public virtual void RemoveItem(T item) 
    {
        items.Remove(item.instanceId);
        currentItemsNum--;
    }
    
    public T GetItem(int instanceID) 
    {
        return items[instanceID];
    }
    
    public bool GetRandomItem(ref T item)
    {
        if (itemsNum > 0)
        {
            int idx = Random.Range(0, itemsNum);
            item = items[items.Keys.ElementAt(idx)];
            return true;
        }
        return false;
    }
}
