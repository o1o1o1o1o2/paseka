﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseController : MonoBehaviour
{
    public Transform doorTransform;

    [SerializeField,ReadOnlyProperty]
    private int _honeyAmount = 0;
    public int honeyAmount => _honeyAmount;

    public void AddHoney(int amount) => _honeyAmount += amount;
}
