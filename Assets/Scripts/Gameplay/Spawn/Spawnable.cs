﻿using System;
using Extra.SimplePool;
using UnityEngine;

[Serializable]
public class Spawnable : ISpawnable, IPoolableData
{

   [SerializeField] private int id;
   
   public int currentNum;
   [SerializeField]
   private PoolIdPair _poolIdPair;
   public PoolIdPair poolIdPair => _poolIdPair;

   [SerializeField]
   private GameObject _prefab;
   public GameObject prefab => _prefab;

   [SerializeField]
   private float _spawnChance;

   public float spawnChance
   {
      get => _spawnChance;
      set => _spawnChance = value;
   }

   public Spawnable(IPoolableData poolableData, float spawnChance)
   {
      _prefab = poolableData.prefab;
      _poolIdPair = poolableData.poolIdPair;
      _spawnChance = spawnChance;
      id = poolIdPair.id;
   }

   public virtual void Spawn()
   {
   }

   public override bool Equals(object obj)
   {
      return ((Spawnable) obj).id == id;
   }
   
   
   public override int GetHashCode()
   {
      return id;
   }
}
