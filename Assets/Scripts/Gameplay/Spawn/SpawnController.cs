using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

 namespace Logic
 {
     public class SpawnController : MonoBehaviour
     {
         private ISpawnable[] _items; 
         
         private BinarySearchComparer _binarySearchComparer;
         
         private Tuple<float,float>[] _itemChancesArr;

         private float _totalChance;

         public void SetItemSpawnChance(int itemID, ISpawnable spawnable)
         {
             _items[itemID] = spawnable;
             SetupSpawnController();
         }

         public void SetupSpawneritems(ISpawnable[] items)
         {
             _items = items;
             SetupSpawnController();
         }

         private void Start()
         {
             _binarySearchComparer = new BinarySearchComparer();
         }

         void SetupSpawnController ()
         {
             if (_items?.Length == 0) throw new Exception("No Items To Spawn");
             
             _totalChance = 0;
             
             foreach (var item in _items)
             {
                 _totalChance += item.spawnChance;
             }

             float beginningOfChance = 0;
             float endOfChance = 0;
             
             _itemChancesArr = new Tuple<float,float>[_items.Length];

             int i = 0;
             foreach (var item in _items)
             {
                 endOfChance = beginningOfChance + item.spawnChance / _totalChance;
                 _itemChancesArr[i] = new Tuple<float,float>(beginningOfChance, endOfChance);
                 beginningOfChance = endOfChance;
                 i++;
             }
         }

         private ISpawnable CurrentSpawn()
         {
             var r = Random.Range(0f, 1f);
             Tuple<float, float> chance = new Tuple<float, float>(r,r);

             int index = Array.BinarySearch(_itemChancesArr, chance, _binarySearchComparer);
             return _items[index];
         }
     
         private class BinarySearchComparer : IComparer<Tuple<float, float>>
         {
             public int Compare( Tuple<float, float> chanceRange, Tuple<float, float> chance)
             {
                 if ( chanceRange?.Item2 <  chance?.Item1) return -1;
            
                 if (chanceRange?.Item1 > chance?.Item1) return 1;
  
                 return 0; 
             }
         }

         [SerializeField,ReadOnlyProperty]
         private float _spawnRate;

         public void SetSpawnRate(float spawnRate)
         {
             _spawnRate = spawnRate;
         }

         public void StartSpawn(float spawnRate)
         {
             _spawnRate = spawnRate;
             StartCoroutine(SpawnC());
         }
         
         public void StopSpawn()
         {
             StopAllCoroutines();
         }

         IEnumerator SpawnC()
         {
             while (true)
             {
                 yield return new WaitForSeconds(_spawnRate);
                 OnSpawn();
             }   
         }
         
         private void OnSpawn()
         {
             CurrentSpawn().Spawn();
         }
     }
 }
