﻿using System.Runtime.CompilerServices;
using UnityEngine;

public class BeeWorkerController : BeeController
{
    [SerializeField,ReadOnlyProperty]
    private bool fullTank;
    private FlowerController _flowerController;
    [SerializeField] private int honeyCapacity;
    
    [SerializeField]
    private int _honeyGatherAmount;
    
    [SerializeField]
    private float _offsetToFlower;
    [SerializeField]
    private float _gatherRateAtFullEfficiency;
    [SerializeField,ReadOnlyProperty]
    private int _currentHoneyAmount;
    [SerializeField,ReadOnlyProperty]
    float _currentGatherRate = 0;

    private float _gatheringTimeEnded;
    [SerializeField, ReadOnlyProperty]
    private bool _gathering;
    private bool _returningToHive;

    public void SetFlowerTarget(FlowerController flowerController)
    {
        _returningToHive = false;
        _flowerController = flowerController;
        var position = _flowerController._gatheringTransform.position;
        var targetWithOffset =  position - (position - _transform.position).normalized*_offsetToFlower;
        SetTarget(targetWithOffset);
    }
    
    private void FindNewFlowerInLateUpdate() => FindTargetFlowerManager.Instance.AddBeeToArrayFindFlower(_transform.position, this);
    
    public void ReturnToHive()
    {
        _returningToHive = true;
        SetTarget(hiveController.spawnZonePosition);
    }
    
    private void Update()
    {
        //if flower has died during gathering or flying to it
        if (!fullTank && !_flowerController.isAlive)
        {
            if (FlowerManager.Instance.itemsNum > 0)
            {
                _gathering = false;
                movingToTarget = false;
                FindNewFlowerInLateUpdate();
                return;
            }
            if (!_returningToHive)
            {
                if (_gathering) _gathering = false;
                ReturnToHive();
            }
        }
        
        if (movingToTarget)
        {
            if (Vector3.Dot(_transform.forward, target - _transform.position) > 0) _transform.position += _transform.forward * (Time.deltaTime * currentMoveSpead);
            else
            {  
                if (fullTank || _returningToHive)
                {
                    hiveController.AddHoney(_currentHoneyAmount);
                    hiveController.WorkerBeeHasReturned();
                    ReturnToPool();
                }
                else
                {
                    movingToTarget = false;
                    if (_flowerController.AddBee())
                    {
                        _gatheringTimeEnded = Time.time + _currentGatherRate;
                        _gathering = true;
                    }
                    else
                    {
                        if (FlowerManager.Instance.GetRandomItem(ref _flowerController)) SetFlowerTarget(_flowerController);
                        else ReturnToHive();
                    }
                }
            }
        }
        
        if (_gathering && Time.time > _gatheringTimeEnded) GatherHoney();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void GatherHoney()
    {
        _currentHoneyAmount += _flowerController.GiveHoney(Mathf.Min(_honeyGatherAmount, honeyCapacity - _currentHoneyAmount));;

        if (_currentHoneyAmount == honeyCapacity)
        {
            fullTank = true;
            _gathering = false;
            _flowerController.RemoveBee();
            ReturnToHive();
        }
        else _gatheringTimeEnded = Time.time + _currentGatherRate;
    }

    protected override void OnPoolDequeue()
    {
        fullTank = false;
        _currentGatherRate = _gatherRateAtFullEfficiency/hiveController.efficiency;
        _currentHoneyAmount = 0;
        FlowerManager.Instance.GetRandomItem(ref _flowerController);
        SetFlowerTarget(_flowerController);
    }

    protected override void Die()
    {
        if (_gathering)
        {
            _flowerController.RemoveBee();
            _gathering = false;
        }
        hiveController.WorkerDied(poolIdPair);
        ReturnToPool();
    }
    
}
