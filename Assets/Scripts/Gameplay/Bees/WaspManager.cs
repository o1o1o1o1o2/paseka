﻿using System.Collections;
using UnityEngine;

public class WaspManager : SpawnManager<WaspController,WaspManager>
{
    [SerializeField] private GameEntitySO waspGameEntity;

    protected override void Awake()
    {
        base.Awake();
        StartCoroutine(SpawnWasps());
    }

    private IEnumerator SpawnWasps()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnRate);
            if (maxItemsNum > currentItemsNum)
            {
                var newWasp = SimplePool.Instance.GetFromPool(waspGameEntity).prefab.GetComponent<WaspController>();
                AddItem(newWasp);
                var spawnPos = GetRandomPositionAtTheEdgeOfRect(_spawnZoneRect); 
                newWasp.gameObject.transform.position = new Vector3(spawnPos.x, 0, spawnPos.y);
                newWasp.Spawn();
            }
        }
    }

    public Vector3 GetNewTarget()
    {
        if (!FlowerManager.Instance.GetRandomFlowerPosition(out Vector3 target)) 
            target = new Vector3(Random.Range (spawnZoneRect.xMin, spawnZoneRect.xMax),0 ,Random.Range (spawnZoneRect.yMin, spawnZoneRect.yMax));
        return target;
    }

    private Vector2 GetRandomPositionAtTheEdgeOfRect(Rect rect)
    {
        int area = Mathf.FloorToInt(Random.Range (0, 3));
 
        //  0   1
        //    2   
        Vector2 pos;
        switch (area)
        {
            case 0:
            {
                pos.x = rect.xMin;
                pos.y = Random.Range (rect.yMin, rect.yMax);
                break;
            }
            case 1:
            {
                pos.x = rect.xMax;
                pos.y = Random.Range (rect.yMin, rect.yMax);
                break;
            }
            default:
            { 
                pos.x = Random.Range (rect.xMin, rect.xMax);
                pos.y = rect.yMin;
                break;
            }
        }
        
        return pos;
    }
}
