using System;
using Extra.SimplePool;
using UnityEngine;

[Serializable]
public class BeeController : EntitieController, IDamageble, IPoolable<HiveController>
{
    [SerializeField]
    protected float moveSpeedFullEfficiency = 1;

    protected Vector3 target;
    public int currentHealth{ get; protected set; }
    public int maxHealth { get; protected set; }

    protected HiveController hiveController;

    public void SetHiveController (HiveController hiveController) => this.hiveController = hiveController;

    [SerializeField,ReadOnlyProperty]
    protected float currentMoveSpead;

    protected bool movingToTarget;

    protected virtual void Die() { }

    public void SetTarget(Vector3 _target)
    {
        target = _target;
        var heading =  target - _transform.position;
        _transform.rotation = Quaternion.LookRotation(heading, Vector3.up);
        movingToTarget = true;
    }

    public virtual void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;
        if (currentHealth <= 0) Die();
    }

    protected void ReturnToPool()
    {
        movingToTarget = false;
        StopAllCoroutines();
        SimplePool.Instance.ReturnToPool(this);
    }

    public void OnPoolDequeue(in HiveController hiveController)
    {
        currentHealth = 1;
        this.hiveController = hiveController;
        currentMoveSpead = moveSpeedFullEfficiency * this.hiveController.efficiency;
        _transform.position = this.hiveController.spawnZonePosition;
        OnPoolDequeue();
    }

    protected virtual void OnPoolDequeue() { }
}
