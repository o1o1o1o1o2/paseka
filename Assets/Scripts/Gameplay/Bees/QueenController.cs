﻿using System.Collections;
using System.Linq;
using Logic;
using UnityEngine;

[RequireComponent(typeof(SpawnController))]
public class QueenController : BeeController
{
    private SpawnController _spawnController;

    [SerializeField] private int sendWorkerBeeAmount = 1;
    [SerializeField] private float sendWorkerBeeRate = 0.1f;
    [SerializeField] private int beesOutPerFlower;
    
    [SerializeField] private float beeSpawnRate = 0.5f;

    protected override void Awake()
    {
        base.Awake();
        if (hiveController == null) hiveController = GetComponentInParent<HiveController>();
        StartCoroutine(SendBeeForHoney());
        InitSpawner();
    }
    
    private void InitSpawner()
    {
        _spawnController = GetComponent<SpawnController>();
        _spawnController.SetupSpawneritems(hiveController.beeSpawnableDic.Values.ToArray());
        SpawnBees();
   
    }
    
    private IEnumerator SendBeeForHoney()
    {
        while (true)
        {
            yield return new WaitForSeconds(sendWorkerBeeRate);
        
            if (!hiveController.HiveIsFullOfHoney() && (float)hiveController.workerBeesOut / FlowerManager.Instance.itemsNum < beesOutPerFlower && hiveController.efficiency > 0.5f)
                hiveController.SendBees(hiveController.workerID,sendWorkerBeeAmount);
        }
    }
    
    private void SpawnBees() => _spawnController.StartSpawn(beeSpawnRate);

    private void StopSpawnBees() => _spawnController.StopSpawn();
}
