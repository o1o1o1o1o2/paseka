﻿using System;
using Extra.SimplePool;
using UnityEngine;


[Serializable]
public class BeeSpawnable : Spawnable
{
    [SerializeField,ReadOnlyProperty]
    private HiveController _hiveController;
    public BeeSpawnable(IPoolableData poolableData, float spawnChance,HiveController hiveController):base(poolableData, spawnChance)
    {
        _hiveController = hiveController;
    }
    
    public override void Spawn() => _hiveController.AddBee(poolIdPair);
    
}
