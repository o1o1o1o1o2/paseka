﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

public class FindTargetFlowerManager: Singleton<FindTargetFlowerManager>
{
    private NativeList<float3> _beePositions;
    private List<BeeController> _needTarget;
    private FindTargetSystem _findTargetSystem;

    public void AddBeeToArrayFindFlower(float3 position, BeeController controller)
    {
        _beePositions.Add(position);
        _needTarget.Add(controller);
    }
    
    public void ClearArrays()
    {
        _beePositions.Clear();
        _needTarget.Clear();
    }

    private void Awake()
    {
        _findTargetSystem = new FindTargetSystem();
        _needTarget = new List<BeeController>();
        _beePositions = new NativeList<float3>(Allocator.Persistent);
    }

    private void OnApplicationQuit()
    {
        _beePositions.Dispose();
    }

    private void LateUpdate()
    {
        if (_needTarget.Count > 0)
        {
            if (FlowerManager.Instance.itemsNum > 0)
            {
                var targets = new NativeArray<int>(_beePositions.Length, Allocator.TempJob);
                _findTargetSystem.FindTarget(FlowerManager.Instance.GetFlowerPositions(), _beePositions.AsArray(), ref targets);

                for (int i = 0; i < targets.Length; i++)
                {
                    ((BeeWorkerController) _needTarget[i]).SetFlowerTarget(FlowerManager.Instance.GetItem(targets[i]));
                }

                targets.Dispose();
            }
            else
            {
                for (int i = 0; i < _needTarget.Count; i++)
                {
                    ((BeeWorkerController) _needTarget[i]).ReturnToHive();
                }
            }
            ClearArrays();
        }
    }
}
