﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

public class FindTargetSystem 
{

    [BurstCompile(CompileSynchronously = true)]
    private struct SetTargetJob : IJobParallelFor
    {
        [ReadOnly] public NativeHashMap<int,float3> searchHashmap;
        [ReadOnly] public NativeArray<float3> positions;

        [NativeDisableParallelForRestriction]
        public NativeArray<int> targets;

        public void Execute(int index)
        {
            NativeArray<int> searchArrIds = searchHashmap.GetKeyArray(Allocator.Temp);
            NativeArray<float3> searchArrPositions = searchHashmap.GetValueArray(Allocator.Temp);
            NativeArray<float> distances = new NativeArray<float>(searchArrPositions.Length, Allocator.Temp);
            
            for (int i = 0; i < distances.Length; i++) distances[i] = math.distance(positions[index], searchArrPositions[i]);
            
            int minDistanceIdx = 0;
            float minDistance = 100;
            for (int i = 0; i < distances.Length; i++)
            {
                
                if (minDistance > distances[i])
                {
                    minDistance = distances[i];
                    minDistanceIdx = i;
                }
            }

            targets[index] = searchArrIds[minDistanceIdx];
        }
    }
    
    public void FindTarget (NativeHashMap<int,float3> searchHashmap, NativeArray<float3> positions, ref NativeArray<int> targets)
    {
        JobHandle setTargetJobHandle = new SetTargetJob()
        {
            positions = positions,
            searchHashmap = searchHashmap,
            targets = targets,
        }.Schedule(targets.Length, 1);
    
        setTargetJobHandle.Complete();
    }
}
