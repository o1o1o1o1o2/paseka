﻿using UnityEngine;

public abstract class EntitieController : MonoBehaviour
{
    public int instanceId { get; private set; }
    protected Transform _transform;
    public PoolIdPair poolIdPair { get; private set;}
    public GameObject prefab => gameObject;

    protected virtual void Awake()
    {
        _transform = transform;
        instanceId = GetInstanceID();
    }
    

    public void InitPoolData(PoolIdPair poolIdPair)
    {
        this.poolIdPair = poolIdPair;
    }
    
}
