﻿using Extra.SimplePool;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class WaspController : EntitieController, IPoolable
{
    private Rigidbody _rigidbody;
    [SerializeField] private float moveSpeed;
    private Vector3 _target;

    [SerializeField] private float killChance;

    protected override void Awake()
    {
        base.Awake();
        _rigidbody = GetComponent<Rigidbody>();
    }

    public void Spawn() => SetTarget();

    private void SetTarget()
    {  
        _target = WaspManager.Instance.GetNewTarget();
        var heading =  _target - _transform.position;
        _transform.rotation = Quaternion.LookRotation(heading, Vector3.up);
    }

    private void Update()
    {
        if (Vector3.Dot(_transform.forward, _target - _transform.position) > 0) _transform.position += _transform.forward * (Time.deltaTime * moveSpeed);
        else SetTarget();
    }

    private void OnTriggerEnter(Collider other) => other.GetComponent<IDamageble>().TakeDamage(1);
    
}
