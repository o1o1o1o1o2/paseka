using Extra.SimplePool;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnableEntity", menuName = "Entities/SpawnableEntity", order = 1)]
public class GameEntitySO : ScriptableObject, IPoolableData
{
    public PoolIdPair poolIdPair { get; private set; }
   
    [SerializeField]
    private GameObject _prefab;

    public GameObject prefab
    {
        get => _prefab;
        set
        {
            if (string.IsNullOrEmpty(_Name)) _Name = _prefab.name;
            poolIdPair = new PoolIdPair( _Name,Animator.StringToHash(_Name));
            _prefab = value;
        }
    }

    [SerializeField] private string _Name;
    
    [SerializeField] 
    protected float _spawnChance;

    public float spawnChance => _spawnChance;

    protected virtual void OnEnable()
    {
        if (_Name == null)
        {
            if (prefab != null)
            {
                _Name = prefab.name;
                poolIdPair = new PoolIdPair( _Name,Animator.StringToHash(_Name));
                return;
            }
            _Name = "";
        }
        poolIdPair = new PoolIdPair( _Name,Animator.StringToHash(_Name));
    }  
}
