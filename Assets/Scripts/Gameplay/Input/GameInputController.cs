﻿using UnityEngine;

public class GameInputController : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    private BeeKeeperController _beeKeeperController;
    private Vector2 touchBeginPos;

    private const string BeekeperTag = "Beekeeper";
    private const string HiveTag = "Hive";

    private void Awake() =>  _beeKeeperController = gameManager.beeKeeperController;

    private void OnEnable()
    {
        TouchInputManager.Instance.TouchBeginEvent += OnTouchBegin;
        TouchInputManager.Instance.TouchEndedEvent += OnTouchEnded;
    }

    private void OnTouchBegin(Touch touch) => touchBeginPos = touch.position;

    private void OnTouchEnded(Touch touch)
    {
        if (Vector2.Distance(touchBeginPos, touch.position) < 0.1)
        {
            var ray = Camera.main.ScreenPointToRay(touch.position);
            var hits = Physics.RaycastAll(ray);
            foreach (var hit in hits)
            {
                if (hit.collider.CompareTag(HiveTag))
                {
                    var hiveController = hit.collider.GetComponent<HiveController>();
                    gameManager.SelectHive(hiveController);
                    return;
                }
                if (hit.collider.CompareTag(BeekeperTag))
                {
                    if (!gameManager.beekeeperSelected) gameManager.SelectBeekeper();
                    else gameManager.DeselectAll();
                    return;
                }
            }

            if (gameManager.beekeeperSelected) _beeKeeperController.MoveToScreenPos(touch.position);
            if (gameManager.selectedHive) gameManager.SelectHive(null);
        }
    }
}
