﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{

    [SerializeField] private Transform ground;
    private Vector3 _groundScale;

    private Camera _mainCam;
    private Vector3 _cameraCenter;
    private Transform _transform;

    private Coroutine _zoomRoutine = null;

    [SerializeField] private float minSize;
    [SerializeField] private float maxSize;
    [SerializeField] private float zoomSpeed;


    private bool _dragging;
    private Vector3 _velocity;
    private Vector3 _camPrevPos;
    private Vector3 _offset;
    
    [SerializeField] private float speed = 1;
    [SerializeField] private float decelerationRate = 0.135f;

    private void Awake()
    {
        Application.targetFrameRate = -1;
        QualitySettings.vSyncCount = 0;
        _mainCam = GetComponent<Camera>();
        _transform = transform;
        _cameraCenter = _transform.position;
        _groundScale = ground.localScale;
    }

    private void OnEnable()
    {
        TouchInputManager.Instance.ZoomEvent += OnZoom;
        TouchInputManager.Instance.TouchBeginEvent += OnTouchBegin;
        TouchInputManager.Instance.TouchProcessEvent += OnTouchProcess;
        TouchInputManager.Instance.TouchEndedEvent += OnTouchEnded;
    }

    private void OnTouchBegin(Touch touch)
    { 
        StopCoroutine(InertiaMove());
        _dragging = true;
        _camPrevPos = _transform.position;
        _velocity = Vector3.zero;
    }

    Vector3 _enterNew, _enterPrev;

    private void OnTouchProcess(Touch touch)
    {
        if (!_dragging) return;

        _enterPrev = _mainCam.ScreenToWorldPoint((touch.position - touch.deltaPosition));
        _enterNew = _mainCam.ScreenToWorldPoint(touch.position);
        _offset = _enterNew - _enterPrev;
        _offset = new Vector3(_offset.x,0,_offset.z*4);
        var position = _transform.position;
        position -= _offset;
        _transform.position = position;
        ClampCamera();
        Vector3 newVelocity = (position - _camPrevPos) / Time.unscaledDeltaTime;
        newVelocity = new Vector3(newVelocity.x,0,newVelocity.z);
        _velocity = Vector3.Lerp(_velocity, newVelocity, Time.unscaledDeltaTime * speed);
        _camPrevPos = position;
        
    }

    private void OnTouchEnded(Touch touch)
    {
        _dragging = false;
        StartCoroutine(InertiaMove());
    }

    private float _xMaxOffset, _zMaxOffset, _camHeight;
    private Vector3 _pos;
    private void ClampCamera()
    {
        _camHeight = 2f * _mainCam.orthographicSize;
        
        _xMaxOffset = (_groundScale.x - (_camHeight * _mainCam.aspect))/2;
        _zMaxOffset = _groundScale.z/2 - _camHeight;
        
        _pos = _transform.position;

        if (_pos.x <= -_xMaxOffset)
        {
            _pos.x = -_xMaxOffset;
            _velocity.x = 0;
        }
        else if (_pos.x >= _xMaxOffset)
        {
            _pos.x = _xMaxOffset;
            _velocity.x = 0;
        }
        
        if (_pos.z <= _cameraCenter.z - _zMaxOffset)
        {
            _pos.z = _cameraCenter.z - _zMaxOffset;
            _velocity.z = 0;
        }
        else if (_pos.z >= _cameraCenter.z + _zMaxOffset)
        {
            _pos.z = _cameraCenter.z + _zMaxOffset;
            _velocity.z = 0;
        }
        
        _transform.position = _pos;
    }
    

    private IEnumerator InertiaMove()
    {
        while (_velocity != Vector3.zero)
        {
            if (!_dragging)
            {
                Vector3 position = _transform.position;
                if (_velocity != Vector3.zero)
                {
                    for (int axis = 0; axis < 3; axis++)
                    {
                        _velocity[axis] *= Mathf.Pow(decelerationRate, Time.unscaledDeltaTime);
                        if (Mathf.Abs(_velocity[axis]) < 1) _velocity[axis] = 0;
                        position[axis] += _velocity[axis] * Time.unscaledDeltaTime;
                    }
                }

                if (_transform.position != position)
                {
                    _transform.position = position;
                    _camPrevPos = _transform.position;
                }
                ClampCamera();
            }
            yield return null;
        }
    }

    private void OnZoom(float y)
    {
        if (_zoomRoutine != null) StopCoroutine(_zoomRoutine);
        _zoomRoutine = StartCoroutine(Grow(y * zoomSpeed));
    }

    private IEnumerator Grow(float y)
    {
        float i = 0;
        float startSize = _mainCam.orthographicSize;
        float endSize;
        if (y > 0) endSize = Mathf.Max(minSize, _mainCam.orthographicSize - y);
        else endSize = Mathf.Min(maxSize, _mainCam.orthographicSize - y);
        while (true)
        {
            i += Time.unscaledDeltaTime / 0.2f;

            _mainCam.orthographicSize = Mathf.Lerp(startSize, endSize, i);
            ClampCamera();

            if (i > 1f)
            {
                yield break;
            }

            yield return null;
        }
    }
}
