public interface IDamageble
{
    int currentHealth { get;}

    int maxHealth{ get;}

   void TakeDamage(int damageAmount);
}
