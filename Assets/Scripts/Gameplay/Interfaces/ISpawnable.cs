public interface ISpawnable 
{
    float spawnChance { get; }

    void Spawn();

}
