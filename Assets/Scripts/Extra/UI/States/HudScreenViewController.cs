﻿public class HudScreenViewController : IState
{
    private readonly HudScreenView _hudScreenView;
    
    private int _beesNum; 
    private int _hivesNum; 
    private int _beesOutNum;
    private int _honeyAmount;

    public bool showStatistic = false;
    
    private GameManager _gameManager;

    public HudScreenViewController(GameManager gameManager, HudScreenView hudScreenView)
    {
        _gameManager = gameManager;
        _hudScreenView = hudScreenView;
        _hudScreenView.ButtonShowStatisticAddListner(OnShowStatisticButtonPressed);
    }

    public void Tick()
    {
        var beesNum = 0;
        var hivesNum = 0;
        var beesOutNum = 0;
        var honeyAmount = 0;
        foreach (var hive in _gameManager.hiveControllers)
        {
            
            if (hive.hiveIsActive)
            {
                beesNum += hive.currentBeeNum;
                beesOutNum += hive.workerBeesOut;
                hivesNum++;
            }
        }

        honeyAmount = _gameManager.houseController.honeyAmount;

        if (beesNum != _beesNum)
        {
            _beesNum = beesNum;
            _hudScreenView.SetBeesNum(_beesNum);
        }
        if (hivesNum != _hivesNum)
        {
            _hivesNum = hivesNum;
            _hudScreenView.SetHivesNum(_hivesNum);
        }
        if (beesOutNum != _beesOutNum)
        {
            _beesOutNum = beesOutNum;
            _hudScreenView.SetBeesOutNum(_beesOutNum);
        }
        if (honeyAmount != _honeyAmount)
        {
            _honeyAmount = honeyAmount;
            _hudScreenView.SetHoneyAmount(_honeyAmount);
        }
    }
    
    private void OnShowStatisticButtonPressed() => showStatistic = true;

    public void OnEnter()
    {
        _hudScreenView.UpdateSpeedSliderValue();
        _hudScreenView.Enable();
    }

    public void OnExit()
    {
        showStatistic = false;
        _hudScreenView.Disable();
    }
}
