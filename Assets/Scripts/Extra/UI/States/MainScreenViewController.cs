﻿using UnityEngine;

public class MainScreenViewController : IState
{
    private readonly MainScreenView _mainScreenView;
    
    public MainScreenViewController(MainScreenView mainScreenView)
    {
        _mainScreenView = mainScreenView;
        _mainScreenView.StartButtonAddListner(StartGame);
    }

    public bool startGame = false;

    private void StartGame()
    {
        Time.timeScale = 1f;
        startGame = true;
    }
    
    public void Tick() {}

    public void OnEnter() =>_mainScreenView.Enable();

    public void OnExit()
    {
        _mainScreenView.Disable();
        startGame = false;
    }
}
