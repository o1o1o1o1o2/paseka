﻿using UnityEngine;

public class StatisticScreenViewController : IState
{
    private GameManager _gameManager;
    private readonly StatisticScreenView _statisticScreenView;
    private float _timescale;

    private readonly GameObject _hiveViewPrefab;
    private readonly HiveView[] _hiveViews;
    public StatisticScreenViewController(GameManager gameManager, StatisticScreenView statisticScreenView)
    {
        _gameManager = gameManager;
        _statisticScreenView = statisticScreenView;
        _statisticScreenView.ButtonBackAddListner(OnButtonBackPressed);
        _hiveViewPrefab = PrefabsDatabase.Instance.hiveViewPrefab;
        _hiveViews = new HiveView[_gameManager.hiveControllers.Length];
        
        for (int i = 0; i < _gameManager.hiveControllers.Length; i++)
        {
            _hiveViews[i] = Object.Instantiate(_hiveViewPrefab).GetComponent<HiveView>();
            _hiveViews[i].Init(_gameManager.hiveControllers[i],OnHiveSelected);
            _statisticScreenView.SetHiveViewParent(_hiveViews[i]);
        }
    }
    
    public bool goBack = false;
    private void OnButtonBackPressed()
    {
        Time.timeScale = 1;
        goBack = true;
    }
    
    public void Tick() { }

    private void UpdateHiveViews()
    {
        for (int i = 0; i < _gameManager.hiveControllers.Length; i++)
        {
            _hiveViews[i].UpdateView(_gameManager.hiveControllers[i]);
        }
    }

    private void OnHiveSelected(HiveController hiveController)
    {
        _gameManager.SelectHive(hiveController);
        goBack = true;
    }

    public void OnEnter()
    {
        UpdateHiveViews();
        _timescale = Time.timeScale;
        Time.timeScale = 0;
        _statisticScreenView.Enable();
    }

    public void OnExit()
    {
        goBack = false;
        Time.timeScale = _timescale;
        _statisticScreenView.Disable();
    }
}
