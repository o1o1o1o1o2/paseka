﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HiveView : MonoBehaviour
{
    private Button _selectButton;
    [SerializeField]
    private TextMeshProUGUI workersNum;
    [SerializeField]
    private TextMeshProUGUI dronesNum;
    [SerializeField]
    private TextMeshProUGUI hornetsNum;
    [SerializeField]
    private Image efficiency;
    [SerializeField]
    private Image honeyAmount;

    private const string StrI = "{0}";

    private void Awake()
    {
        _selectButton = GetComponent<Button>();
    }

    public void Init(HiveController hiveController, UnityAction<HiveController> callBack)
    {
        _selectButton.onClick.AddListener(() => callBack(hiveController));
    }

    public void UpdateView(HiveController hiveController)
    {
        workersNum.SetText(StrI, hiveController.workersNum);
        dronesNum.SetText(StrI, hiveController.dronesNum);
        hornetsNum.SetText(StrI, hiveController.hornetsNum);
        efficiency.fillAmount = hiveController.efficiency;
        if (hiveController.efficiency > 0.5f) efficiency.color = Color.Lerp(Color.yellow, Color.green,  hiveController.efficiency*2 - 1);
        else efficiency.color = Color.Lerp(Color.red, Color.yellow, hiveController.efficiency*2);
        honeyAmount.fillAmount = (float)hiveController.currentHoneyAmount / hiveController.honeyCapacity;
    }
}
