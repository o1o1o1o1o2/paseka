﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MainScreenView : View
{
    [SerializeField]
    private Button startButton;

    public void StartButtonAddListner(UnityAction method) => startButton.onClick.AddListener(method);
    public void StartButtonRemoveListner(UnityAction method) => startButton.onClick.RemoveListener(method);
}
