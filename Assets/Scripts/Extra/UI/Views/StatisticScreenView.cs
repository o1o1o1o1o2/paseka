﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StatisticScreenView : View
{
    [SerializeField] private VerticalLayoutGroup hiveViewGroup;
    [SerializeField] private Button buttonBack;

    public void ButtonBackAddListner(UnityAction method) => buttonBack.onClick.AddListener(method);
    public void ButtonBackRemoveListner(UnityAction method) => buttonBack.onClick.RemoveListener(method);

    public void SetHiveViewParent(HiveView hiveView)
    {
        hiveView.transform.SetParent(hiveViewGroup.transform);        
    }
}
