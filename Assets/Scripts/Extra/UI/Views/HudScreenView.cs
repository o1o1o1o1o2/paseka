﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HudScreenView : View
{
    [SerializeField] private Button buttonShowStatistic;

    [SerializeField] private TextMeshProUGUI hivesNumText;
    [SerializeField] private TextMeshProUGUI beesNumText;
    [SerializeField] private TextMeshProUGUI beesOutText;
    [SerializeField] private TextMeshProUGUI honeyAmountText;

    private string intString = "{0}";
    
    [SerializeField] private Slider gameSpeed;

    private void Start()
    {
        gameSpeed.value = Time.timeScale;
        gameSpeed.onValueChanged.AddListener(OnGameSpeedChange);
    }

    public void UpdateSpeedSliderValue() => gameSpeed.value = Time.timeScale;
    public void SetBeesNum(int num) => beesNumText.SetText(intString, num);
    public void SetHivesNum(int num) =>hivesNumText.SetText(intString, num);
    public void SetBeesOutNum(int num) =>beesOutText.SetText(intString, num);
    public void SetHoneyAmount(int num) =>honeyAmountText.SetText(intString, num);
    private void OnGameSpeedChange(float speed) => Time.timeScale = speed;
    
    public void ButtonShowStatisticAddListner(UnityAction method) => buttonShowStatistic.onClick.AddListener(method);
}
