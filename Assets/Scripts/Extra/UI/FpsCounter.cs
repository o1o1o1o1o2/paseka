﻿using TMPro;
using UnityEngine;

 
public class FpsCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _fpsText;
    [SerializeField] private float _hudRefreshRate = 1f;
 
    private float _timer;
    
    float deltaTime = 0.0f;

    private const string s = "Fps:{0}";
 
    private void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        if (Time.unscaledTime > _timer)
        {
            _fpsText.SetText(s,1.0f/deltaTime);
            _timer = Time.unscaledTime + _hudRefreshRate;
        }
    }
}