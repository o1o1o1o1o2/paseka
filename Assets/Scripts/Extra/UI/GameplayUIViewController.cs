﻿using UnityEngine;
public class GameplayUIViewController
{
    private GameManager _gameManager;
    private GameplayUIView _gameplayUIView;
    private bool _interactionPanelEnabled;
    private bool _statisticPanelEnabled;

    public GameplayUIViewController(GameManager gameManager, GameplayUIView gameplayUIView)
    {
        _gameManager = gameManager;
        _gameplayUIView = gameplayUIView;
        _gameplayUIView.ButtonCleanHiveAddListner(OnCleanHiveButtonPressed);
        _gameplayUIView.ButtonGatherAddListner(OnGatherButtonPressed);
        _statisticPanelEnabled = false;
        _interactionPanelEnabled = false;
    }
    
    public void Tick()
    {
       if (_gameManager.selectedHive) UpdateHivePanelPosition();
       else
       {
           if (_interactionPanelEnabled)
           {
               _gameplayUIView.DisableHiveInteractionPanel();
               _interactionPanelEnabled = false;
           }

           if (_statisticPanelEnabled)
           {
               _gameplayUIView.DisableHiveStatisticPanel();
               _statisticPanelEnabled = false;
           }
       }
    }
    
    private void UpdateHivePanelPosition()
    {
        if (_gameManager.beekeeperSelected)
        {
            if (!_interactionPanelEnabled)
            {
                _gameplayUIView.EnableHiveInteractionPanel();
                _interactionPanelEnabled = true;
                _gameplayUIView.DisableHiveStatisticPanel();
                _statisticPanelEnabled = false;
            }
            var screenPosition = Camera.main.WorldToScreenPoint(_gameManager.selectedHive.transform.position + Vector3.up);
            _gameplayUIView.SetHiveIteractionPanelPosition(screenPosition);
        }
        else
        {
            if (!_statisticPanelEnabled)
            {
                _gameplayUIView.EnableHiveStatisticPanel();
                _statisticPanelEnabled = true;
                _gameplayUIView.DisableHiveInteractionPanel();
                _interactionPanelEnabled = false;
            }
            _gameplayUIView.SetupUICameraPosition(_gameManager.selectedHive.transform);
            var screenPosition = Camera.main.WorldToScreenPoint(_gameManager.selectedHive.transform.position + Vector3.up);
            _gameplayUIView.SetHiveStatisticPanelPosition(screenPosition);
            _gameplayUIView.UpdateHiveView(_gameManager.selectedHive);
        }
    }
    
    private void OnCleanHiveButtonPressed() =>_gameManager.beeKeeperController.CleanHive(_gameManager.selectedHive);
    
    private void OnGatherButtonPressed() => _gameManager.beeKeeperController.GatherHoney(_gameManager.selectedHive);

}
