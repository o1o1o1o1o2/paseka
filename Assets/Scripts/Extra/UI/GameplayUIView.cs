﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameplayUIView : MonoBehaviour
{
    [SerializeField] private Camera uiCamera;
    private Transform _uiCameraTransform;
    [SerializeField] private Button buttonCleanHive;
    [SerializeField] private Button buttonGather;
    
    private RectTransform _selectedHiveIteractionPanelRT;
    private RectTransform _selectedHiveStatisticPanelRT;

    private HiveView _hiveView;

    [SerializeField]
    private Canvas hiveInteractionCanvas;
    [SerializeField]
    private Canvas hiveStatisticCanvas;

    private void Awake()
    {
        _hiveView = hiveStatisticCanvas.gameObject.GetComponent<HiveView>();
        _selectedHiveStatisticPanelRT = hiveStatisticCanvas.GetComponent<RectTransform>();
        _selectedHiveIteractionPanelRT = hiveInteractionCanvas.GetComponent<RectTransform>();
        DisableHiveStatisticPanel();
        DisableHiveInteractionPanel();
        _uiCameraTransform = uiCamera.transform;
    }

    public void EnableHiveStatisticPanel()
    {
        uiCamera.enabled = true;
        hiveStatisticCanvas.enabled = true;
    }

    public void DisableHiveStatisticPanel()
    {
        uiCamera.enabled = false;
        hiveStatisticCanvas.enabled = false;
    }

    public void EnableHiveInteractionPanel() => hiveInteractionCanvas.enabled = true;
    public void DisableHiveInteractionPanel() => hiveInteractionCanvas.enabled = false;

    public void ButtonCleanHiveAddListner(UnityAction method) => buttonCleanHive.onClick.AddListener(method);
    
    public void ButtonGatherAddListner(UnityAction method) => buttonGather.onClick.AddListener(method);

    public void SetHiveIteractionPanelPosition(Vector2 position) => _selectedHiveIteractionPanelRT.position = position;
    public void SetHiveStatisticPanelPosition(Vector2 position) => _selectedHiveStatisticPanelRT.position = position;

    public void UpdateHiveView(HiveController hiveController) => _hiveView.UpdateView(hiveController);
    
    public void SetupUICameraPosition(Transform _transform)
    {
        var rot = Quaternion.Euler(45, - _transform.forward.z * 45, 0);
        _uiCameraTransform.rotation = rot;
        _uiCameraTransform.position = _transform.position - _uiCameraTransform.forward*3f + Vector3.up*0.5f;
    }

}
