﻿using System;

namespace Extra.SerializableDictionary.MyDictionaries
{
    [Serializable] public class DictionaryBeeSpawnable : SerializableDictionary<PoolIdPair, BeeSpawnable>  
    {
    }
    [Serializable] public class DictionaryFlowerSpawnable : SerializableDictionary<PoolIdPair, FlowerSpawnable>  
    {
    }
    
}