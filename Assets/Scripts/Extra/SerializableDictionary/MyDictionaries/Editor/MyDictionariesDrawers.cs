﻿using UnityEditor;
using UnityEngine;

namespace Extra.SerializableDictionary.MyDictionaries.Editor
{
    [CustomPropertyDrawer(typeof(DictionaryBeeSpawnable))]
    public class DictionaryBeesNumDrawer : DictionaryDrawer<PoolIdPair, BeeSpawnable>
    {
        private GameEntitySO[] obj;
        protected override void DragAndDropItems(SerializedProperty property)
        {
            obj = new GameEntitySO[DragAndDrop.objectReferences.Length];
            for (int i = 0; i < DragAndDrop.objectReferences.Length; i++)
            {
                obj[i] = DragAndDrop.objectReferences[i] as GameEntitySO;
                if (!_Dictionary.ContainsKey(obj[i].poolIdPair)) _Dictionary.Add(obj[i].poolIdPair, new BeeSpawnable(obj[i],obj[i].spawnChance,null));
            }
        }
        
        protected override dynamic GetViewItemKey(PoolIdPair item)
        {
            if (item.Name == null) item.Name = "";
            return item.Name;
        }

        protected override void UpdateKey<T>(ref PoolIdPair item, T viewItem) { }
        
        protected override void AddNewItemButton(Rect buttonRect, SerializedProperty property) { }

        protected override void ClearDictionaryButton(Rect buttonRect, SerializedProperty property) { }

        protected override bool RemoveItemButton(Rect removeRect, SerializedProperty property, ref PoolIdPair key)
        {
            return false;
        }

        protected override void DrawValueField(Rect valueRect, ref BeeSpawnable value)
        {
            valueRect.x += 5; 
            valueRect.xMax = valueRect.x + valueRect.width / 2;
            value.spawnChance = EditorExt.DoField(valueRect, value.spawnChance.GetType(), value.spawnChance);
            valueRect.x = valueRect.xMax + 10;
            valueRect.xMax -= 15;
            value.currentNum = EditorExt.DoField(valueRect, value.currentNum.GetType(), value.currentNum);
        }
    }

    [CustomPropertyDrawer(typeof(DictionaryFlowerSpawnable))]
    public class DictionaryFlowerSpawnableDrawer : DictionaryDrawer<PoolIdPair, FlowerSpawnable> 
    {
        private FlowerSO[] obj;
        protected override void DragAndDropItems(SerializedProperty property)
        {
            obj = new FlowerSO[DragAndDrop.objectReferences.Length];
            for (int i = 0; i < DragAndDrop.objectReferences.Length; i++)
            {
                obj[i] = DragAndDrop.objectReferences[i] as FlowerSO;
                if (!_Dictionary.ContainsKey(obj[i].poolIdPair)) _Dictionary.Add(obj[i].poolIdPair, new FlowerSpawnable(obj[i],obj[i].spawnChance,obj[i].flowerSpawnData));
            }
        }
        
        protected override dynamic GetViewItemKey(PoolIdPair item)
        {
            if (item.Name == null) item.Name = "";
            return item.Name;
        }

        protected override void UpdateKey<T>(ref PoolIdPair item, T viewItem) { }

        protected override void DrawValueField(Rect valueRect, ref FlowerSpawnable value)
        {
            valueRect.x += 5; 
            valueRect.xMax = valueRect.x + valueRect.width / 2;
            value.spawnChance = EditorExt.DoField(valueRect, value.spawnChance.GetType(), value.spawnChance);
            valueRect.x = valueRect.xMax + 10;
            valueRect.xMax -= 15;
            value.currentNum = EditorExt.DoField(valueRect, value.currentNum.GetType(), value.currentNum);
        }

        protected override void AddNewItemButton(Rect buttonRect, SerializedProperty property)
        {
        }
       
    }
}