﻿using System;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


[CustomPropertyDrawer(typeof(SerializableDictionary<,>))]
public abstract class DictionaryDrawer<TK, TV> : PropertyDrawer
{
   
    protected SerializableDictionary<TK, TV> _Dictionary;
    protected bool _Foldout;
    protected const float kButtonWidth = 22f;
    protected const int kMargin = 3;
   
    static GUIContent iconToolbarMinus = EditorGUIUtility.IconContent("Toolbar Minus", "Remove selection from list");
    static GUIContent iconToolbarPlus = EditorGUIUtility.IconContent("Toolbar Plus", "Add to list");
    static GUIStyle preButton = "RL FooterButton";
    static GUIStyle boxBackground = "RL Background";
 
 
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        CheckInitialize(property, label);
        if (_Foldout)
            return Mathf.Max((_Dictionary.Count + 1) * 17f, 17 + 16) + kMargin * 2;
        return 17f + kMargin * 2;
    }

    protected virtual void DragAndDropItems(SerializedProperty property)
    {
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        
        Event e = Event.current;
        EventType eType = e.type;
        DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
        if (position.Contains(e.mousePosition) && eType == EventType.DragExited)
        {
            DragAndDropItems(property);
                DidChangeProperty(property);
            
        }

        CheckInitialize(property, label);
 
        position.height = 17f;
 
        var foldoutRect = position;
        foldoutRect.width -= 2 * kButtonWidth;
        EditorGUI.BeginChangeCheck();

        _Foldout = EditorGUI.Foldout(foldoutRect, _Foldout, label, true);
        if (EditorGUI.EndChangeCheck()) {
            EditorPrefs.SetBool(label.text, _Foldout);
            DidChangeProperty(property);
        }
 
        var buttonRect = position;
        buttonRect.x = position.width - kButtonWidth + position.x;
        buttonRect.width = kButtonWidth + 2;
 
        AddNewItemButton(buttonRect,property);

        buttonRect.x -= kButtonWidth;

        ClearDictionaryButton(buttonRect, property);
        
        if (!_Foldout)
            return;

        DrawDic(position, property);
     }

     protected virtual dynamic GetViewItemKey(TK item)
     {
         return item;
     }
     
     protected virtual void UpdateKey<T>(ref TK item,T viewItem)
     {
         item =  (TK)Convert.ChangeType(viewItem, typeof(TK));
     }
    

    protected void RemoveItem(TK key)
    {
        _Dictionary.Remove(key);
    }
 
    protected virtual void CheckInitialize(SerializedProperty property, GUIContent label)
    {
        if (_Dictionary == null)
        {
            var target = property.serializedObject.targetObject;
            _Dictionary = fieldInfo.GetValue(target) as SerializableDictionary<TK, TV>;
            if (_Dictionary == null)
            {
                _Dictionary = new SerializableDictionary<TK, TV>();
                fieldInfo.SetValue(target, _Dictionary);
            }
 
            _Foldout = EditorPrefs.GetBool(label.text);
        }
    }

    protected virtual void ClearDictionaryButton(Rect buttonRect, SerializedProperty property)
    {
        if (GUI.Button(buttonRect, new GUIContent("X", "Clear dictionary"), EditorStyles.miniButtonRight)) {
            ClearDictionary();
            DidChangeProperty(property);
        }
    }

    protected virtual void AddNewItemButton(Rect buttonRect, SerializedProperty property)
    {
        if (GUI.Button(buttonRect, new GUIContent("+", "Add item"), EditorStyles.miniButton)) {
            AddNewItem();
            DidChangeProperty(property);
        }
    }

    protected virtual dynamic DrawKeyField(Rect keyRect, TK key)
    {
        var viewKey = GetViewItemKey(key);
        viewKey = EditorExt.DoField(keyRect, viewKey.GetType(), viewKey);
        UpdateKey(ref key, viewKey);
        return key;
    }

    protected virtual bool DrawKey(Rect keyRect, SerializedProperty property, TK key, TV value)
    {
        EditorGUI.BeginChangeCheck();

        var newKey = DrawKeyField(keyRect, key);

        if (EditorGUI.EndChangeCheck()) {
            try {
                _Dictionary.Remove(key);
                _Dictionary.Add(newKey, value);
            } catch (Exception e) {
                Debug.Log(e.Message);
            }
            DidChangeProperty(property);
            return true;
        }
        return false;
    }
    
    protected virtual void DrawValueField(Rect valueRect, ref TV value)
    {
        value = EditorExt.DoField(valueRect, typeof(TV), value);
    }
    
    protected virtual bool DrawValue(Rect valueRect, SerializedProperty property, ref TK key, ref TV value)
    {
        EditorGUI.BeginChangeCheck();
        DrawValueField(valueRect,ref value);
        if (EditorGUI.EndChangeCheck()) {
            _Dictionary[key] = value;
            DidChangeProperty(property);
            return true;
        }
        return false;
    }

    protected virtual void DrawDic(Rect position, SerializedProperty property)
    {
        foreach (var item in _Dictionary) {
            var key = item.Key;
            var value = item.Value;
 
            position.y += 17f;
 
            var keyRect = position;
            keyRect.width /= 2;
            keyRect.width -= 4;
 
            if (DrawKey(keyRect,property,key,value)) break; 

            var valueRect = position;
            valueRect.x = position.width / 2 + 15;
            valueRect.width = keyRect.width - kButtonWidth;
            
            if (DrawValue(valueRect,property, ref key,ref value)) break;
            
            var removeRect = valueRect;
            removeRect.x = valueRect.xMax + 2;
            removeRect.width = kButtonWidth;

            if (RemoveItemButton(removeRect, property, ref key)) break;
        }
    }

    protected virtual bool RemoveItemButton(Rect removeRect, SerializedProperty property, ref TK key)
    {
        if (GUI.Button(removeRect, new GUIContent("x", "Remove item"), EditorStyles.miniButtonRight)) {
            RemoveItem(key);
            DidChangeProperty(property);
            return true;
        }
        return false;
    }


    protected void ClearDictionary()
    {
        _Dictionary.Clear();
    }
 
    protected virtual void AddNewItem()
    {
        TK key;
        if (typeof(TK) == typeof(string)) key = (TK)(object)"";
        else key = default;
        Debug.Log(key);
 
        var value = default(TV);
        try
        {
            _Dictionary.Add(key, value);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }
    
    protected void DidChangeProperty(SerializedProperty property) {
        var target = property.serializedObject.targetObject;
        PrefabUtility.RecordPrefabInstancePropertyModifications(target);
        EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
    }
}




 