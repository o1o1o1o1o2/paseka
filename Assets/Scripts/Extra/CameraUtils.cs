﻿using UnityEngine;

namespace Fly.Utils
{
    public static class CameraUtils
    {
        public static bool ScreenPointToHitWorldPosition(this Camera cam, Vector3 position, out Vector3 hit)
        {
            Ray ray = cam.ScreenPointToRay(position);

            RaycastHit _hit;
            if (Physics.Raycast(ray.origin, ray.direction, out _hit, 700))
            {
                hit = _hit.point;
                return true;
            }
            hit = Vector3.zero;
            return false;
        }

        public static bool ScreenPointToHitWorldPosition(this Camera cam, Vector3 position, out Vector3 hit, int layermask)
        {
            Ray ray = cam.ScreenPointToRay(position);
            RaycastHit _hit;
            if (Physics.Raycast(ray.origin, ray.direction, out _hit, 700, layermask))
            {
                hit = _hit.point;
                return true;
            }
            hit = Vector3.zero;
            return false;
        }

        public static bool ScreenPointToHitsWorldPositions(this Camera cam, Vector3 position, out Vector3[] hitPoints)
        {
            Ray ray = cam.ScreenPointToRay(position);
            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray, 256f);

            hitPoints = new Vector3[hits.Length];

            if (hits.Length == 0) return false;

            for (int i = 0; i < hits.Length; i++)
            {
                hitPoints[i] = hits[i].point;
            }

            return true;
        }

        public static bool ScreenPointToHitLocalPosition(this Camera cam, Vector3 position, out Vector3 hit)
        {
            Ray ray = cam.ScreenPointToRay(position);

            RaycastHit _hit;
            if (Physics.Raycast(ray.origin, ray.direction, out _hit, 1000))
            {
                hit = _hit.transform.InverseTransformPoint(_hit.point);
                return true;
            }
            hit = Vector3.zero;
            return false;
        }


        public static Vector3 ViewportCenterToHitPlaneZXWithHeightY(this Camera cam, float belowY)
        {
            Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            RaycastHit[] hits;
            hits = Physics.RaycastAll(ray);

            float sliceHeight = 0;
            float maxHeight = 0;
            for (int i = 0; i < hits.Length; i++)
            {
                maxHeight = hits[i].point.y;
                if (maxHeight <= belowY && maxHeight > sliceHeight) sliceHeight = maxHeight;
            }

            float distance;
            Plane xz = new Plane(Vector3.up, new Vector3(0f, sliceHeight, 0f));
            xz.Raycast(ray, out distance);
            return  ray.GetPoint(distance);
        }


    }
}
