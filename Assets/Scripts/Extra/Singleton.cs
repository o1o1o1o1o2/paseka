using System;
using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T _instance;
    
    private static object _lock = new object();

    public static T Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    var instances = FindObjectsOfType<T>();

                    if (instances.Length != 0) _instance = instances[0];

                    if (instances.Length > 1)
                    {
                        Debug.LogError("There is more than one " + typeof(T).Name + " in the scene, deleteing");

                        for (int i = 1; i < instances.Length; i++)
                        {
                            Destroy(instances[i]);
                        }
                    }

                    if (_instance == null)
                    {
                        Debug.Log("Singleton not found " + typeof(T) + "CreatingNew");
                        _instance = new GameObject(typeof(T).Name).AddComponent<T>();
                    }
                }
                return _instance;
            }
        }
    }
    
    private static bool applicationIsQuitting = false;

    public void OnDestroy()
    {
        applicationIsQuitting = true;
    }

    public static bool IsDestroy()
    {
        return applicationIsQuitting;
    }
}
