using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshSurface))]
public class RebuildNavMeshAtRuntime : MonoBehaviour
{
    
    private NavMeshData _meshData;
    private NavMeshSurface _navMeshSurface;

    private void Awake()
    {
        _navMeshSurface = GetComponent<NavMeshSurface>();
        _meshData = _navMeshSurface.navMeshData;
    }

    public void Rebuild()
    {
        _navMeshSurface.UpdateNavMesh(_meshData);
    }
   
    public void SetNavMeshSize()
    {
       // nm.size = new Vector3(GameSettings.Instance.gridSize.x,0,GameSettings.Instance.gridSize.y) + new Vector3(GameSettings.Instance.Offset,0, GameSettings.Instance.Offset);
    }
}
