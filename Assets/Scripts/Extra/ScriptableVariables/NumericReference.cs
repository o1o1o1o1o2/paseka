using System;
using UnityEngine;

[Serializable]
public class NumericReference<T> where T : struct
{
    public bool useConstant = true;

    public T constantValue;

    public NumericReference() { }
    public NumericReference(T value)
    {
        useConstant = true;
        constantValue = value;
    }

}

[Serializable]
public class FloatReference : NumericReference<float>
{
    public FloatVariable variable;
    public FloatReference(float value) : base(value) { }
    public FloatReference() { }
    
    public float Value => useConstant ? constantValue : variable.RuntimeValue;
    public static implicit operator float(FloatReference reference)
    {
        return reference.Value;
    }
    public static implicit operator FloatReference(float value)
    {
        return new FloatReference(value);
    }
}

[Serializable]
public class IntReference : NumericReference<int>
{
    public IntVariable variable;
    public IntReference(int value) : base(value) { }
    public IntReference() { }
    public int Value => useConstant ? constantValue : variable.RuntimeValue;
    public static implicit operator int(IntReference reference)
    {
        return reference.Value;
    }
    public static implicit operator IntReference(int value)
    {
        return new IntReference(value);
    }
}

[Serializable]
public class Vector3Reference : NumericReference<Vector3>
{
    public Vector3Variable variable;
    public Vector3Reference(Vector3 value) : base(value) { }
    public Vector3Reference() { }
    public Vector3 Value => useConstant ? constantValue : variable.RuntimeValue;
    public static implicit operator Vector3(Vector3Reference reference)
    {
        return reference.Value;
    }
    public static implicit operator Vector3Reference(Vector3 value)
    {
        return new Vector3Reference(value);
    }
}

[Serializable]
public class BoolReference : NumericReference<bool>
{
    public BoolVariable variable;
    public BoolReference(bool value) : base(value) { }
    public BoolReference() { }
    public bool Value => useConstant ? constantValue : variable.RuntimeValue;
    public static implicit operator bool(BoolReference reference)
    {
        return reference.Value;
    }
    public static implicit operator BoolReference(bool value)
    {
        return new BoolReference(value);
    }
}




