using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(NumericReference<>), true)]
public class NumericReferenceDrawer<T> : PropertyDrawer where T : struct
{
    private readonly string[] _PopupOption = {"Use Constant", "Use Variable"};
    
    private GUIStyle _PopupStyle;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (_PopupStyle == null)
        {
            _PopupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
            _PopupStyle.imagePosition = ImagePosition.ImageOnly;
        }

        var labelPosition = position;
        label = EditorGUI.BeginProperty(position, label, property);
        position.xMin = 0;
        position.xMax = labelPosition.width/4;
        EditorGUI.LabelField(position, label);
        
        EditorGUI.BeginChangeCheck();
        
        SerializedProperty useConstant = property.FindPropertyRelative("useConstant");
        SerializedProperty constantValue = property.FindPropertyRelative("constantValue");
        
      
        position.xMin = position.xMax;
        position.xMax = position.xMax+30;
        Rect buttonRect = new Rect(position);
        buttonRect.yMin += _PopupStyle.margin.top;
        buttonRect.width = _PopupStyle.fixedWidth + _PopupStyle.margin.right;
        position.xMin = buttonRect.xMax;
        
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, _PopupOption, _PopupStyle);
        useConstant.boolValue = result == 0;
     
        position.xMax = labelPosition.xMax;
        if (useConstant.boolValue) EditorGUI.PropertyField(position,  constantValue, GUIContent.none);
        else
        {
            NumericReference<T> serializedObj = SerializedPropertyExtensions.SerializedPropertyToObject<NumericReference<T>>(property);

            if (serializedObj is FloatReference)
            {
                FloatReference numericReference = SerializedPropertyExtensions.SerializedPropertyToObject<FloatReference>(property);
                if (numericReference.variable == null) numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(FloatVariable), true) as FloatVariable;
                else
                {
                    position.xMax = labelPosition.width/2;
                    numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(FloatVariable), true) as FloatVariable;
                    if (numericReference.variable != null)
                    {
                        position.xMin = position.xMax + 5;
                        position.xMax = labelPosition.xMax;
                        numericReference.variable.InitialValue =
                            SerializedPropertyExt.MyFloatFieldInternal(position, new Rect(position.x - 10, position.yMin, 10, position.height), numericReference.variable.InitialValue, EditorStyles.numberField);
                        numericReference.variable.RuntimeValue = numericReference.variable.InitialValue;
                        EditorUtility.SetDirty(numericReference.variable);
                    }
                }
            }
            else if (serializedObj is IntReference)
            {
                IntReference numericReference = SerializedPropertyExtensions.SerializedPropertyToObject<IntReference>(property);
                if (numericReference.variable == null) numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(IntVariable), true) as IntVariable;
                else
                {
                    position.xMax = labelPosition.width/2;
                    numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(IntVariable), true) as IntVariable;
                    if (numericReference.variable != null)
                    {
                        position.xMin = position.xMax + 5;
                        position.xMax = labelPosition.xMax;
                        numericReference.variable.InitialValue =
                            SerializedPropertyExt.MyIntFieldInternal(position, new Rect(position.x - 10, position.yMin, 10, position.height), numericReference.variable.InitialValue, EditorStyles.numberField);
                        numericReference.variable.RuntimeValue = numericReference.variable.InitialValue;
                        EditorUtility.SetDirty(numericReference.variable);
                    }
                }
            }
            else if (serializedObj is Vector3Reference)
            {
                Vector3Reference numericReference = SerializedPropertyExtensions.SerializedPropertyToObject<Vector3Reference>(property);
                if (numericReference.variable == null) numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(Vector3Variable), true) as Vector3Variable;
                else
                {
                    position.xMax = labelPosition.width/2;
                    numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(Vector3Variable), true) as Vector3Variable;
                    if (numericReference.variable != null)
                    {
                        position.xMin = position.xMax + 5;
                        position.xMax = labelPosition.xMax;
                        numericReference.variable.InitialValue = EditorGUI.Vector3Field(position, GUIContent.none, numericReference.variable.InitialValue);
                        numericReference.variable.RuntimeValue = numericReference.variable.InitialValue;
                        EditorUtility.SetDirty(numericReference.variable);
                    }
                }
            }
            else if (serializedObj is BoolReference)
            {
                BoolReference numericReference = SerializedPropertyExtensions.SerializedPropertyToObject<BoolReference>(property);
                if (numericReference.variable == null) numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(BoolVariable), true) as BoolVariable;
                else
                {
                    position.xMax = labelPosition.width/2;
                    numericReference.variable = EditorGUI.ObjectField(position, numericReference.variable, typeof(BoolVariable), true) as BoolVariable;
                    if (numericReference.variable != null)
                    {
                        position.xMin = position.xMax + 5;
                        position.xMax = labelPosition.xMax;
                        numericReference.variable.InitialValue = EditorGUI.Toggle(position, GUIContent.none, numericReference.variable.InitialValue);
                        numericReference.variable.RuntimeValue = numericReference.variable.InitialValue;
                        EditorUtility.SetDirty(numericReference.variable);
                    }
                }
            }
        }

        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();

    }
}

[CustomPropertyDrawer(typeof(FloatReference), true)]
public class FloatVariableReferenceDrawer : NumericReferenceDrawer<float>
{
}

[CustomPropertyDrawer(typeof(IntReference), true)]
public class IntVariableReferenceDrawer : NumericReferenceDrawer<int>
{
}

[CustomPropertyDrawer(typeof(Vector3Reference), true)]
public class Vector3VariableReferenceDrawer : NumericReferenceDrawer<Vector3>
{
}

[CustomPropertyDrawer(typeof(BoolReference), true)]
public class BoolVariableReferenceDrawer : NumericReferenceDrawer<bool>
{
}

