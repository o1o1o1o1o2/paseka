using System;
using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable", menuName = "SO/IntVariable", order = 1)]
[Serializable]
public class IntVariable : ScriptableVariable<int>
{
}

