using UnityEngine;

public static class RandomEXT 
{
    public static Vector2 GetRandomPositionAtTheEdgeOfScreen(Vector2 screenSize)
    {
        int area = Mathf.FloorToInt(Random.Range (0, 4));
        //   1
        //  0  2
        //    3   

        Vector2 pos;
        switch (area)
        {
            case 0:
            {
                pos.x = 0;
                pos.y = Random.Range (0, screenSize.y);
                break;
            }
            case 1:
            {
                pos.x = Random.Range (0, screenSize.x);
                pos.y = screenSize.y;
                break;
            }
            case 2:
            {
                pos.x = screenSize.x;
                pos.y = Random.Range (0, screenSize.y);
                break;
            }
            default:
            { 
                pos.x = Random.Range (0, screenSize.x);
                pos.y = 0;
                break;
            }
        }
        
        return pos;
    }
    
    public static Vector2 GetRandomPositionAtTheEdgeOfRect(Rect rect)
    {
        int area = Mathf.FloorToInt(Random.Range (0, 4));
        //   1
        //  0  2
        //    3   

        Vector2 pos;
        switch (area)
        {
            case 0:
            {
                pos.x = rect.xMin;
                pos.y = Random.Range (rect.yMin, rect.yMax);
                break;
            }
            case 1:
            {
                pos.x = Random.Range (rect.xMin, rect.xMax);
                pos.y = rect.yMax;
                break;
            }
            case 2:
            {
                pos.x = rect.xMax;
                pos.y = Random.Range (rect.yMin, rect.yMax);
                break;
            }
            default:
            { 
                pos.x = Random.Range (rect.xMin, rect.xMax);
                pos.y = rect.yMin;
                break;
            }
        }
        
        return pos;
    }
}
