﻿using System;
using System.Runtime.CompilerServices;

[Serializable]
public struct PoolIdPair
{
    public string Name;
    public int id;
    
    public PoolIdPair(string name, int id)
    {
        Name = name;
        this.id = id;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator string(PoolIdPair e) { return e.Name; }
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator int(PoolIdPair e) { return e.id; }
    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator PoolIdPair(string e) { return new PoolIdPair(e,0); }    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static implicit operator PoolIdPair(int e) { return new PoolIdPair("",e); }


    public override bool Equals(object obj)
    {
        return ((PoolIdPair) obj).id == id;
    }


    public override int GetHashCode()
    {
        return id;
    }
}
