using UnityEngine;

namespace Extra.SimplePool
{
    public interface IPoolableData
    {
        PoolIdPair poolIdPair { get;}
        GameObject prefab { get; }
    }

    public interface IPoolable: IPoolableData 
    {
        void InitPoolData(PoolIdPair poolIdPair);
    }


    public interface IPoolable<T>: IPoolable where T : class
    {
        void OnPoolDequeue(in T hiveController);
    }
}