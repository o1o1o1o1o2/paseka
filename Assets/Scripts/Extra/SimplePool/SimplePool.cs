using System;
using System.Collections.Generic;
using Extra.SimplePool;
using UnityEngine;

public class SimplePool : Singleton<SimplePool>
{
   private Dictionary<int, Tuple<Transform,Queue<IPoolableData>>> poolDictionary;

   private void Awake()
   {
      poolDictionary = new Dictionary<int, Tuple<Transform,Queue<IPoolableData>>>();
   }

   public IPoolableData GetFromPool(IPoolableData poolItem)
   {
      if (!poolDictionary.ContainsKey(poolItem.poolIdPair))
      {
         var poolGO = new GameObject(poolItem.poolIdPair.Name + "Pool");
         poolGO.transform.parent = transform;
         poolDictionary.Add(poolItem.poolIdPair, new Tuple<Transform, Queue<IPoolableData>>(poolGO.transform,new Queue<IPoolableData>()));
      }

      IPoolable obj;
      
      if (poolDictionary[poolItem.poolIdPair].Item2.Count != 0)
      {
         obj = (IPoolable)poolDictionary[poolItem.poolIdPair].Item2.Dequeue();
         obj.prefab.SetActive(true);
         return obj;
      }
      
      obj = Instantiate(poolItem.prefab, poolDictionary[poolItem.poolIdPair].Item1).GetComponent<IPoolable>();
      obj.InitPoolData(poolItem.poolIdPair);
      return obj;
   }

   public void ReturnToPool(IPoolableData poolItem)
   {
      poolItem.prefab.SetActive(false);
      poolDictionary[poolItem.poolIdPair].Item2.Enqueue(poolItem);
   }
}
